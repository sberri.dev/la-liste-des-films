const movieList = [
    {
        title: "Avatar",
        releaseYear: 2009,
        duration: 162,
        director: "James Cameron",
        actors: ["Sam Worthington", "Zoe Saldana", "Sigourney Weaver", "Stephen Lang"],
        description: "A paraplegic marine dispatched to the moon Pandora on a unique mission becomes torn between following his orders and protecting the world he feels is his home.",
        poster: "https://m.media-amazon.com/images/M/MV5BZDA0OGQxNTItMDZkMC00N2UyLTg3MzMtYTJmNjg3Nzk5MzRiXkEyXkFqcGdeQXVyMjUzOTY1NTc@._V1_SX300.jpg",
        rating: 7.9,
    },
    {
        title: "300",
        releaseYear: 2006,
        duration: 117,
        director: "Zack Snyder",
        actors: ["Gerard Butler", "Lena Headey", "Dominic West", "David Wenham"],
        description: "King Leonidas of Sparta and a force of 300 men fight the Persians at Thermopylae in 480 B.C.",
        poster: "https://m.media-amazon.com/images/M/MV5BMjc4OTc0ODgwNV5BMl5BanBnXkFtZTcwNjM1ODE0MQ@@._V1_SX300.jpg",
        rating: 7.7,
    },
    {
        title: "The Avengers",
        releaseYear: 2012,
        duration: 143,
        director: "Joss Whedon",
        actors: ["Robert Downey Jr.", "Chris Evans", "Mark Ruffalo", "Chris Hemsworth"],
        description: "Earth's mightiest heroes must come together and learn to fight as a team if they are to stop the mischievous Loki and his alien army from enslaving humanity.",
        poster: "https://m.media-amazon.com/images/M/MV5BNDYxNjQyMjAtNTdiOS00NGYwLWFmNTAtNThmYjU5ZGI2YTI1XkEyXkFqcGdeQXVyMTMxODk2OTU@._V1_SX300.jpg",
        rating: 8.1,
    },
];

console.log(movieList)

//Création d'un nouvel objet qui est un nouveau film qui sera ajouter au table ci-dessus
const newMovie = {
    title: "X Men",
    releaseYear: 2000,
    duration: 217,
    director: "James Mangold",
    actors: ["Hugh Jackman", "James McAvoy", "Patrick Steward"],
    description: "En 1963, les X-Men sont un groupe de cinq jeunes étudiants super-héros créés par Stan Lee et Jack Kirby pour Marvel Comics. Ils apprennent à maîtriser leurs pouvoirs, aidés par leur mentor.",
    poster: "https://www.ecranlarge.com/uploads/image/001/022/x-men-affiche-francaise-1022157.jpg",
    rating: 9,
}

movieList.push(newMovie) //Ajout du film à la liste grâce à la méthode "push"

movieList.shift(); //Suppression du premier film de la liste grâce à la méthode "shift"

const filmListElement = document.getElementById("filmList"); // Récupère l'élément HTML avec l'id "filmList"

// Parcourt la liste de films
movieList.forEach((movie) => {

    const filmElement = document.createElement("div"); // Crée un nouvel élément div pour chaque film

    filmElement.classList.add("movie-container") // Ajout d'une classe "movie-container" à l'élément div

    // Remplit le contenu HTML de l'élément div avec les détails du film
    filmElement.innerHTML = `<h3>${movie.title}</h3>
    <div class="cover"><img src=${movie.poster}></div>
    <p>Année: ${movie.releaseYear}</p>
    <p>Durée: ${movie.duration}</p>
    <p>Réalisateur: ${movie.director}</p>
    <p>Acteurs: ${movie.actors}</p>
    <p>Description: ${movie.description}</p>
    <p>Notes: ${movie.rating}</p>`


    filmListElement.appendChild(filmElement) // Ajout de l'élément du film à l'élément HTML avec l'id "filmList"

});

